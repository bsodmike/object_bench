require 'benchmark/ips'

class MyObject
  def initialize(foo, bar)
    @foo = foo
    @bar = bar
  end
end

class MyStruct < Struct.new(:foo, :bar)
end

Benchmark.ips do |x|
  x.report("empty object") { Object.new }
  x.report("new object")   { MyObject.new(:foo, :bar) }
  x.report("new object with string args")   { MyObject.new('foo', 'bar') }
  x.report("new struct")   { MyStruct.new(:foo, :bar) }
  x.report("new array")    { [[:foo, :foo], [:bar, :bar]] }
  x.report("new hash")     { {:foo => :foo, :bar => :bar} }
  x.report("new hash with string keys")     { {'foo' => :foo, 'bar' => :bar} }
end

__END__

        empty object          6.874M (± 3.1%) i/s -     34.383M
          new object          5.042M (± 2.2%) i/s -     25.278M
new object with string args   3.134M (± 3.6%) i/s -     15.725M
          new struct          4.678M (± 2.3%) i/s -     23.470M
           new array          6.331M (± 2.2%) i/s -     31.677M
            new hash          2.289M (± 3.9%) i/s -     11.484M
new hash with string keys     2.066M (± 3.9%) i/s -     10.389M

